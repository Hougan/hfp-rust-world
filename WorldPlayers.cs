using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Oxide.Core;
using Oxide.Core.Libraries;

namespace Oxide.Plugins
{
    [Info("World Players", "0.0.1", "Hougan")]
    [Description("Показывает местоположение игроков и высчитывает средний географический пояс")]
    public class WorldPlayers : RustPlugin
    {
        #region eNums

        private enum Action
        {
            Set,
            Connect,
            Disconnect,
            
            Initialize
        }

        #endregion
        
        #region Classes

        private class Player
        {
            [JsonProperty("displayName")]
            private string DisplayName;
            [JsonProperty("steamId")]
            private ulong SteamID;

            [JsonProperty("ip")]
            private string Ip;

            public Player(BasePlayer player)
            {
                DisplayName = player.displayName;
                SteamID = player.userID;

                Ip = player.net.connection.ipaddress.Split(':')[0];
            }
        }

        private class Response
        {
            public bool Result;
            public string Data;
        }

        private class Cache
        {
            public string Key;
            public int Password;

            public void Write()
            {
                Interface.Oxide.DataFileSystem.WriteObject("WorldPlayers_Cache", this);
            }
            
            public static Cache Read()
            {
                if (!Interface.Oxide.DataFileSystem.ExistsDatafile($"WorldPlayers_Cache"))
                    return Generate();

                Cache cache;
                try
                {
                    cache = Interface.Oxide.DataFileSystem.ReadObject<Cache>("WorldPlayers_Cache");
                }
                catch
                {
                    return Generate();
                }

                return cache;
            }
            
            public static Cache Generate()
            {
                return new Cache
                {
                    Key      = "FAILED",
                    Password = Oxide.Core.Random.Range(0, 9990)
                };
            }
        }
        
        #endregion

        #region Variables

        private Cache CacheData = Cache.Read();

        #endregion

        #region OnServerInitialized

        private void OnServerInitialized()
        {
            RegisterServer();
        }

        #endregion

        #region Hooks

        private void OnPlayerConnected(BasePlayer player)
        {
            Request(Action.Connect, new Player(player), null);
        }

        private void OnPlayerDisconnected(BasePlayer player, string reason = "")
        {
            Request(Action.Disconnect, new Player(player), null);
        }

        #endregion

        #region API

        private void RegisterServer()
        {
            CacheData = Cache.Read();
            
            Request(Action.Initialize, new { pass = CacheData.Password }, response =>
            {
                if (!response.Result)
                {
                    PrintError($"Не удалось зарегистрироваться в сервисе [{response.Data}]");
                    return;
                }

                if (response.Data != CacheData.Key)
                {
                    PrintError($"Ваш АПИ ключ был автоматически изменён! {CacheData.Key} -> {response.Data}");
                    SetSavedKey(response.Data);
                }
                
                PrintError($"Успешно зарегистрировались в сервисе!");
                Puts($"Ссылка: http://89.223.122.156:2020/view/{CacheData.Key}");
                Puts($"Пароль: {CacheData.Password}");

                SendSnapshot();
            });
        } 

        private void SendSnapshot()
        {
            List<Player> snapshotInfos = new List<Player>();

            foreach (var player in BasePlayer.activePlayerList)
            {
                snapshotInfos.Add(new Player(player));
            }
            
            Request(Action.Set, snapshotInfos, response =>
            {
                if (!response.Result)
                {
                    PrintError($"Не удалось отправить список игроков [{response.Data}]");
                    return;
                }
            });
        }

        private void Request(Action mode, object data, Action<Response> callback)
        {            
            string json = JsonConvert.SerializeObject(data);

            const string @baseUri = "http://89.223.122.156:2020/api?"; 
            string @prepareUri = UriEncode($"mode={mode}&key={CacheData.Key}");
            
            webrequest.Enqueue(@baseUri + @prepareUri, json, (code, rawResponse) =>
            {
                try
                { 
                    Response response = JsonConvert.DeserializeObject<Response>(rawResponse);
                    if (!response.Result)
                    {
                        RegisterServer();
                        return;
                    } 
                    
                    callback?.Invoke(response);
                }
                catch
                {
                    LogToFile($"RequestErrors", $"{code} | {rawResponse}\n{json}\n--------------", this);
                    
                    callback?.Invoke(new Response
                    {
                        Result = false,
                        Data = $"{code} | Не удалось создать запрос [{CacheData.Key}]"
                    });
                }
            }, this, RequestMethod.POST, new Dictionary<string, string> { ["Content-Type"] = "application/json" }, 1f);
        }

        #endregion

        #region Cache

        private void SetSavedKey(string key)
        {
            CacheData.Key = key;
            
            CacheData.Write();
        }

        #endregion

        #region Utils

        private string UriEncode(string input)
        {
            while (input.Contains("#")) input  = input.Replace("#", "%23");
            while (input.Contains("$")) input  = input.Replace("$", "%24");
            while (input.Contains("+")) input  = input.Replace("+", "%2B");
            while (input.Contains("/")) input  = input.Replace("/", "%2F");
            while (input.Contains("\\")) input = input.Replace("\\", "%23");
            while (input.Contains(":")) input  = input.Replace(":", "%3A");
            while (input.Contains(";")) input  = input.Replace(";", "%3B"); 
            while (input.Contains("?")) input  = input.Replace("?", "%3F");
            while (input.Contains("@")) input  = input.Replace("@", "%40");
            
            return input;
        }

        #endregion
    }
}