// Глобально регистрируем алиасы.
require('module-alias/register');
const moment = require('moment-timezone');

(async () => {
    // Создаём подключение к базе данных
    await require('@backend/plugins/input');

    global.app = {};
    global.logger = require('@backend/plugins/logger');
    global.axios = require('axios');

    const timezone = 'Asia/Krasnoyarsk'

    const base = moment().tz('Europe/Moscow');
    const connect = moment().tz(timezone);

    /**
     * Заставляем подключиться к базе данных, и только затем - подключаем оставшиеся скрипты
     */
    await require('@backend/plugins/database')(() => {
        // Подключаем все скрипты
        require('@backend/index')();
    });
})();
