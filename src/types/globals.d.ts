// -------------------------------------------------------------------------
// Расширение существующих пространств имен и интерфейсов.
// -------------------------------------------------------------------------

declare namespace NodeJS {
    interface Global {
        app: App;
        logger: import('winston').Logger;
        axios: import('axios');
    }
}

// -------------------------------------------------------------------------
// Новые основоположные типы
// -------------------------------------------------------------------------

interface App {
    webServer?: import('http').Server;
    express?: import('express').Express;
    socketServer?: import('socket.io').Server;
}

interface Window {
    router: any,
    webApi: any,
}

interface PlayerInfo {
    displayName: string;
    steamId: string;

    ip: string;
    coords: ([number,number]|null);
    marker: (null|ymaps.Placemark);
    line: (null|ymaps.Polyline);
    avatar: string;
    
    offset: (number|null);
}

interface DefaultPacket {
    key: string;
    pass: string;
}

interface ServerInterface {
    ip: string;
    apiKey: string;
    coords: ([number,number]|null);

    players: PlayerInfo[];
}

interface GeoCode {
    coords: ([number, number]|null);
    timezone: (number|null);
}

// -------------------------------------------------------------------------
// Объявление констант
// -------------------------------------------------------------------------
declare const app: App;
declare const logger: import('winston').Logger;
declare const axios: import('axios').AxiosStatic;

(<any>window).router = import('vue-router');