const md5 = require('md5');
const moment = require('moment-timezone');

/** Класс предназначен для управления MongoDB. */
class Server {
    /**
     * Список всех подключенных серверов
     * @type {Server[]}
     */
    static serversPoll = [];

    /**
     * Количество сделанных запросов за последнюю минуту
     * @type {number}
     */
    static get requestAmount() {
        return this._requestAmount || 0;
    }

    static set requestAmount(v) {
        this._requestAmount = v;
    }

    /**
     * Получить сервер по ключу
     * @param {string} api
     */
    static findServer(api) {
        return this.serversPoll.find(value => value.apiKey === api);
    }

    /**
     * Ответить на запрос
     * @param {boolean} result
     * @param {any} data
     */
    static answer(result, data) {
        return JSON.stringify({Result: result, Data: data});
    }

    
    /**
     * Обработчик количества ожидающих запросов
     */
    static queryAmount() {
        let total = 0;

        this.serversPoll.forEach(value => {
            value.players.forEach(player => {
                if (!player.coords) {
                    total++;
                }
            })
        });

        return total;
    }


    /**
     * Получить координаты пользователя по IP
     * @param {string} value
     * @param {boolean} shake
     * @returns {Promise<(GeoCode|boolean)>}
     */
    static async geoCode(value, shake = false, uri = 'http://ip-api.com/json/') {
        const apiId = Server.requestAmount > 40 ? 2 : 1;

        if (Server.requestAmount >= 115) {
            app.socketServer.emit('status update', {
                query: this.queryAmount(),
                apiId: apiId,
                limit: `${Server.requestAmount} / 115`
            });
            logger.error('Слишком много запросов к гео-апи, нужно ждать!');
            return false;
        }

        const result = await axios.get(`${uri}${value}`).catch(err => {});
        Server.requestAmount++;

        if (!result || !result.data) {
            if (uri.startsWith('http://www.geooplugin')) {
                axios.get(`http://api.hougan.space/vk/notify/Skins/API забилось, хыыы/rA:Tx=La74E[`);
                return false;
            }

            return await this.geoCode(value, shake, 'http://www.geoplugin.net/json.gp?ip=');
        }

        let lat = 0, lng = 0, timezone = '';

        switch (uri) {
            case 'http://www.geoplugin.net/json.gp?ip=': {
                lat = Number.parseFloat(result.data.geoplugin_latitude)
                lng = Number.parseFloat(result.data.geoplugin_longitude)
                timezone = result.data.geoplugin_timezone;
                break;
            }
            case 'http://ip-api.com/json/': {
                lat = result.data.lat;
                lng = result.data.lon;
                timezone = result.data.timezone;
                break;
            }
        }

        const timezoneOffset = moment().tz(timezone).utcOffset() - 180;

        if (shake) {
            const offSet = (Math.random() > 0.5 ? 1000 : -1000);
            lat += (Math.random() * offSet) / 1000;
            lng += (Math.random() * offSet) / 1000;
        }

        app.socketServer.emit('status update', {
            query: Server.queryAmount(),
            apiId: apiId,
            limit: `${Server.requestAmount} / 115`
        });

        return {
            coords: [lat , lng],
            timezone: timezoneOffset
        };
    }

    /**
     * Инициализация класса
     * @param {string} apiKey
     * @param {string} ip
     * @param {boolean} create
     * @param {string} pass
     * @returns {Promise<(null|Server)>}
     */
    static async new(apiKey, ip, create, pass) {
        if (!apiKey || apiKey === "FAILED") {
            apiKey = md5(Math.random().toString());
        }

        const result = Server.findServer(apiKey);
        if (!result) {
            if (!create) {
                logger.error(`[${ip} | ${apiKey.substr(0, 10)}] Сервер в системе не найден`);
                return null;
            }
 
            /** @type {([number, number]|null)} */
            let result = null;

            if (!pass) {
                logger.error(`Инициализация сервера без пароля запрещена! [${ip}]`);
                return null;
            }

            while (!Array.isArray(result)) {
                const geoInfo = await Server.geoCode(ip);

                if (typeof geoInfo !== 'boolean'){
                    result = geoInfo.coords;
                }
            }

            
            logger.info(`[${ip} | ${apiKey.substr(0, 10)}] Регистрация сервера в системе`);
            return new this(apiKey, ip, result, pass);
        }

        if (result.ip === ip) {
            logger.info(`[${ip} | ${apiKey.substr(0, 10)}] Авторизация сервера в системе`);
            return result;
        }

        return this.new('', ip, create, pass);
    }

    /**
     * Конструктор
     * @param {string} apiKey
     * @param {string} ip
     * @param {[number, number]} coords
     * @param {string} pass
     */
    constructor(apiKey, ip, coords, pass) {
        this.data = {
            ip,
            apiKey,
            pass,
            coords,

            /** @type {PlayerInfo[]} */
            players: [],
            /** @type {SocketIO.Socket[]} */
            listeners: [],

            /** @type {number} */
            offset: 0
        };
        
        const text = encodeURI(`Сервер вышел на связь ${ip}: ${apiKey}`);
        axios.get(`http://api.hougan.space/vk/notify/Skins/${text}/rA:Tx=La74E[`);
        
        Server.serversPoll.push(this);

        this.geoCodeTick();
    }

    /**
     * АПИ Ключ сервера
     * @type {string}
     */
    get apiKey() { return this.data.apiKey; }

    set apiKey(value) { this.data.apiKey = value; }

    /**
     * Пароль от сервера
     * @type {string}
     */
    get pass() { return this.data.pass }

    set pass(value) { this.data.pass = value; }

    /**
     * Массив игроков сервера
     * @type {PlayerInfo[]}
     */
    get players() { return this.data.players; }

    set players(value) { this.data.players = value; }

    /**
     * IP адрес
     * @type {string}
     */
    get ip() { return this.data.ip; }

    set ip(value) { this.data.ip = value; }

    /**
     * Прослушиватели событий
     * @type {SocketIO.Socket[]}
     */
    get listeners() { return this.data.listeners; };

    set listeners(value) { this.data.listeners = value; }

    /**
     * Координаты сервера
     * @type {[number, number]} value
     */

    get coords() { return this.data.coords; }

    set coords(value) { this.data.coords = value; }

    /**
     * Обработать подключение игрока
     * @param {PlayerInfo} value
     * @param {boolean} broadcast
     */
    connectPlayer(value, broadcast = false) {
        const result = this.players.find(player => player.steamId === value.steamId);

        if (result) {
            logger.error(`[${this.ip} | ${this.apiKey.substr(0, 10)}] Игрок ${value.ip} [${value.steamId}] уже подключён!`);
            return;
        }

        if (broadcast) {
            logger.info(`[${this.ip} | ${this.apiKey.substr(0, 10)}] Игрок ${value.ip} [${value.steamId}] подключился!`);
        }

        this.players.push(value);
        this.loadAvatar(value);

        return Server.answer(true, '');
    }

    /**
     * Загрузить информацию об игроке
     * @param {PlayerInfo} value
     */
    async loadAvatar(value) {
        const regex = /<avatarFull>\n?<!\[CDATA\[(.*)\]\]>/;
        const result = await axios.get(`https://steamcommunity.com/profiles/${value.steamId}?xml=1`);

        const matches = regex.exec(result.data.toString());
        if (!matches) {
            value.avatar = 'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/fe/fef49e7fa7e1997310d705b2a6158ff8dc1cdfeb_full.jpg';
            return;
        }
        value.avatar = matches[1];
    }

    /**
     * Обработать отключение игрока
     * @param {PlayerInfo} value
     */
    disconnectPlayer(value) {
        const result = this.players.find(player => player.steamId === value.steamId);

        if (!result) {
            logger.error(`[${this.ip} | ${this.apiKey.substr(0, 10)}] Игрок ${value.ip} [${value.steamId}] уже отключён!`);
            return;
        }

        // Заставляем отслушивающих удалить игрока
        this.broadcast(-1, value);

        logger.info(`[${this.ip} | ${this.apiKey.substr(0, 10)}] Игрок ${value.ip} [${value.steamId}] отключился!`);
        this.players.splice(this.players.indexOf(result), 1);
        return Server.answer(true, '');
    }

    /** 
     * Обработать установку игрока
     * @param {PlayerInfo[]} value
     */
    setPlayers(value) {
        // Очищаем текущий список игроков
        this.players = [];

        // Заставляем отслушивающих сбросить карту
        this.broadcast(0, null);

        value.forEach(player => this.connectPlayer(player, false));

        logger.info(`[${this.ip} | ${this.apiKey.substr(0, 10)}] Установка ${value.length} игроков в системе`);
        return Server.answer(true, null);
    }

    /**
     * Обработчик гео-кодинга
     */
    async geoCodeTick() {
        const player = this.players.find(value => !value.coords);
        const apiId = Server.requestAmount > 40 ? 2 : 1;

        app.socketServer.emit('status update', {
            query: Server.queryAmount(),
            apiId: apiId,
            limit: `${Server.requestAmount} / 115`
        });

        if (!player) {
            Server.requestAmount = Math.max(Server.requestAmount - 1, 0);
            setTimeout(() => this.geoCodeTick(), 1000);
            return;
        }

        const result = await Server.geoCode(player.ip, true);
        if (typeof result !== 'boolean') {
            //logger.info(`[${this.ip} | ${this.apiKey.substr(0, 10)}] Получены координаты '${player.ip}' [${result}]`);
            player.coords = result.coords;
            player.offset = typeof result.timezone !== 'number' ? null : result.timezone;

            // Заставляем отслушивающих добавить игрока
            this.broadcast(1, player);
        } else {
            Server.requestAmount = Math.max(Server.requestAmount - 4, 0);
            logger.error(`[${this.ip} | ${this.apiKey.substr(0, 10)}] Не удалось получить данные '${result}'`);
        }

        setTimeout(() => this.geoCodeTick(), result ? 500 : 5000);
    }

    /**
     * Подписаться на события сервера
     * @param {SocketIO.Socket} client
     * @returns {boolean}
     */
    isSubscribed(client) {
        const result = this.listeners.find(value => value.id === client.id);

        return !!result;
    }

    /**
     * Отправить обновление всем клиентам
     * @param {number} action
     * @param {any} data
     */
    broadcast(action, data) {
        this.listeners.forEach(value => {
            value.emit('server update', action, data)
        });
    }

    /**
     * Подписаться на события сервера
     * @param {SocketIO.Socket} client
     */
    subscribe(client) {
        if (this.isSubscribed(client)) {
            return;
        }

        logger.info(`На прослушивание событий подписался ${client.conn.remoteAddress}`)
        this.listeners.push(client);
    }

    /**
     * Вернуть информацию о сервере
     */
    info() {
        return Server.answer(true, this.apiKey);
    }
}

module.exports = Server;


