const http = require('http');
const express = require('express');
const socketIo = require('socket.io');

const bodyParser = require('body-parser');
const compression = require('compression');

const { webServer } = require('@root/config');
const { WebApiServer } = require('osmium-webapi');

app.express = express();

app.express.set('trust proxy', 1);
app.express.set('view engine', 'pug');
app.express.set('views', './src/frontend/views');

app.express.use(bodyParser.json());
app.express.use(bodyParser.urlencoded({extended: true}));
app.express.use(express.static(`./public`));
app.express.use(compression());

app.webServer = new http.Server(app.express);

const ioServer = socketIo(app.webServer);

// @ts-ignore
app.socketServer = new WebApiServer(ioServer);
require('@backend/middleware/packet')();

require('@backend/routes')();
Object.entries(require('@backend/events')).forEach(([key, method]) => {
  try {
    method();
    logger.info(`Подключен файл: '${key}'`);
  }
  catch {
    logger.error(`Ошибка подключения файла: '${key}'`);
  }
});

app.webServer.listen(webServer.port);
logger.info(`Веб-сервер на порту ${webServer.port} [ОК]`);