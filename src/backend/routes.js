const { routeLogger } = require('@backend/middleware/routes');
const Server = require('@backend/main/classes/Server');

module.exports = () => {
  if (!app.express){
    logger.error('Экспресс не загружен!');
    return;
  }

  app.express.post('/api', async (req, res) => {
    const mode = req.query.mode.toString().toLowerCase();
    const key = req.query.key.toString();
    const data = req.body || null;

    const ip = req.ip.toString().split('f:')[1];

    let server = await Server.new(key, ip, mode === 'initialize', data.pass);
    if (!server) {
      res.end(Server.answer(false, 'Сервер не авторизован в системе!'));
      return;
    }

    switch (mode) { 
      // Подключение нового сервера к сервису
      case 'initialize': { 
        res.end(server.info());
        break;
      }

      case 'connect': {
        res.end(server.connectPlayer(data));
        break;
      }

      case 'disconnect': {
        res.end(server.disconnectPlayer(data));
        break;
      }

      case 'set': {
        res.end(server.setPlayers(data));
        break;
      }
    }

    return;
  });

  app.express.get('/*', async (req, res) => {
    res.render('main');
    return;
  });
};
