import 'vuetify/dist/vuetify.min.css';
import '@mdi/font/css/materialdesignicons.css';
import '@mdi/font/css/materialdesignicons.min.css';

import App from '@frontend/main.vue';
import Map from '@frontend/components/Map.vue';
import Main from '@frontend/components/main.vue';

import Vue from 'vue';
import Vuetify from 'vuetify'
import IO from 'socket.io-client';
import WebApi from 'osmium-webapi';
import VueRouter from 'vue-router';

Vue.use(Vuetify);
Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [{
    path: '/view/:key',
    component: Map,
  }],
});

window.webApi = new WebApi.WebApiClient(IO('/'));
window.router = router;
new Vue({
  vuetify: new Vuetify({}),
  router,
  render: (h) => h(App),
  async mounted() {
    await window.webApi.ready();

    // @ts-ignore
    this.$children[0].webApi = {};
    // @ts-ignore
    this.$children[0].webApi = window.webApi;

    // @ts-ignore
    if (this.$children[0].webApiReady) {
      // @ts-ignore
      this.$children[0].webApiReady();
    }
  },
}).$mount('#app');
